# frozen_string_literal: true

module RuWEB
  # Gets macros
  module SewMacros
    extend self
    
    def init(macros, cmds: [], in_macro: '', loops: 100)
      @macros = macros
      @pass   = true
      @els    = obtain_els(cmds, in_macro)
      @loops  = loops
      sew
      inspect
      resolve
    end
    
    private
    
    def obtain_els(cmds, macro)
      cmd = [{index: 0, cmd: '', data: "\u005F#{macro}"}]
      cmds.empty? ? cmd : cmds
    end
    
    def sew
      @macros.map do |m|
        @els.map! do |e|
          e.each do |k, v|
            e[k] = e[k].gsub(/\b#{m[:macro]}\b/, m[:data]) if k == :data
          end
        end
      end
    end
    
    def inspect
      @macros.map do |e|
        @pass = false if @els.join.scan(e[:macro]).any?
      end
    end
    
    def resolve
      if @pass
        @els[0][:cmd].empty? ? @els[0][:data] : @els
      else
        inspect_loops
        init(@macros, cmds: @els, loops: @loops - 1)
      end
    end
    
    def inspect_loops
      if @loops <= 0
        puts '🕷️ ruweb: [ERROR] too much recursion: check for missing macros or typos in their declarations or calls'
        abort
      end
    end
  end
end