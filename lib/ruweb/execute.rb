# frozen_string_literal: true

require 'English'

module RuWEB
  # Executes the code
  module Execute
    extend self
    
    def init(code)
      return unless code
      index = 1
      code.each do |e|
        cmd = merge(e[:cmd], e[:data])
        system(cmd)
        next unless $CHILD_STATUS.exitstatus.positive?

        file = "ruweb_log#{index}"
        puts "🕷️ ruweb: something went wrong on top: saving #{file}"
        File.write(file, e[:data])
        index += 1
      end
    end

    private

    def merge(cmd, data)
      cmd =~ /%/ ? cmd.gsub('%', data) : "#{cmd} <<'EOS'\n#{data}\nEOS"
    end
  end
end