# frozen_string_literal: true

require 'open-uri'

module RuWEB
  # Reads the Markdown
  module Read
    extend self
    
    def init(uri)
      File.exist?(uri) ? File.read(uri).strip : URI.parse(uri).open.read.strip
      rescue StandardError
        puts  "🕷️ ruweb: [ERROR] couldn't read '#{uri}': verify its availability or run 'ruweb' for help"
        abort
    end
  end 
end