# 1653809677

* STDOUT readability.
* Bug fix that causes execution error when there are no executables blocks.
* Implementation of discretional macros.

# 1649993986.0

* No pad execution if there is any save option.

#  1628636073.0

* `Sew` now requieres at least one argument.
* Module `SewCode` now can return only macros, cmds or both.
* New module `SewMacros` for methods that were in `SewCode`.
* Renamed parameters from `--download`, `--knit` and `--unknit` to `--save-raw`, `--save-source` and `--save-code`.
* Conceptual corrections where “preprocess”, “knit” and “unknit” are now “sew”, “sew source” and “sew code”.
* Change from class variables to instance variables.
* Change from classes to modules.
* Formatting on help display.

# 1625795090

* Bug fix that causes non-stop loops in some macros substitutions.

# 1624474085

* Macros can be use as parameters of other pads after its call.
* New class `Read` for getting local or remote texts.
* New --download argument for downloading raw text.

# 1623376710

* Macros doesn't have inline comments between parentheses.
* Macros declarations are now with underscore.
* Other pads declarations are now with underscore + link in Markdown.
* Version number and latest change synced to the same timestamp.

# 1620938655

* Lincense now is [Open and Free Publishing License (LEAL)](https://programando.li/bres/).

# 1620894786

* Conceptual correction where “weave” is now “knit” and “unweave” is now “unknit”.

# 1620891507

* Typo fix from “weaved” to “woven”.

# 1620871648

* Bug fixes that didn't allow local execution.

# 1620701079

* MANUAL splitted from README.

# 1620609469

* Conceptual correction where “tangle” is now “unweave”.

# 1620598149

* Since this version, RuWEB successfully build itself from [its pad](https://pad.programando.li/ruweb).